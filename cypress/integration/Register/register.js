import poRegister from "../../fixtures/page_objects/register.json";
import testDataRegister from "../../fixtures/test_data/register.json";
import poDashboard from "../../fixtures/page_objects/dashboard.json";
let GLOBAL_EMAIL = "email" +Math.floor(Math.random() * 1000) + "@gmail.com";
                // = email715@gmail.com

describe('Test suite for registration module', ()=> {
    beforeEach(()=> {
        cy.visit("https://demowebshop.tricentis.com/");
    })

    
    it.only('WITH FIXTURES - verify user is able to perform sign in with new account and when user logout then user email should not be displayed', ()=>{
        cy.get(poDashboard.button_register).click();
        cy.get(poRegister.field_firstName).type("First");
        cy.get(poRegister.field_lastName).type("last");
        cy.get(poRegister.field_email).type(GLOBAL_EMAIL);
        cy.get(poRegister.field_password).type(testDataRegister.valid_password);
        cy.get(poRegister.field_confirm_password).type(testDataRegister.valid_password);
        cy.get(poRegister.button_register).click();
        cy.get(poRegister.error_message_mismatch_password).should('be.visible');
        cy.get(poDashboard.button_logout).click();
        cy.get(poRegister.error_message_mismatch_password).should('not.exist');
    })
})